<?php

/**
 * @file
 * CTools plugin.
 */

$plugin = array(
  'title' => t('Random Videos'),
  'description' => t('Display a random list of videos from specified field instance'),
  'single' => TRUE,
  'render callback' => 'random_videos_render',
  'defaults' => array(),
  'edit form' => 'random_videos_edit_form',
  'category' => array(t('Vimeo Ext')),
);


/**
 * Content type settings form.
 */
function random_videos_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $fields = field_read_fields(array('type' => 'field_vimeo'));
  $options = array();
  foreach ($fields as $key => $value) {
    $options[$key] = $value['field_name'];
  }
  $form['bundle'] = array(
    '#title' => t('Field Instance'),
    '#descriptions' => t('Select field instance to get random videos'),
    '#type' => 'select',
    '#options' => $options,
  );

  return $form;
}


/**
 * Settings form submit handler.
 */
function random_videos_edit_form_submit($form, &$form_state) {
  $form_state['conf']['bundle'] = $form_state['values']['bundle'];
}


/**
 * Renders content type using standard block object
 * @param type $subtype
 * @param type $conf
 * @param type $args
 * @param type $context
 * @return \stdClass $block
 */
function random_videos_render($subtype, $conf, $args, $context) {
  $nids = db_select('field_data_' . $conf['bundle'], 'v')
      ->fields('v', array('entity_id'))
      ->range(0, 3)
      ->orderRandom()
      ->execute()
      ->fetchCol();

  $nodes = node_load_multiple($nids);
  $ren_array = node_view_multiple($nodes, 'teaser');

  $block = new stdClass();
  $block->title = t('Random Videos');
  $block->content = drupal_render($ren_array);

  return $block;
}
